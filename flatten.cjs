function flatten(elements, depth) {
  if (typeof depth === "undefined") {
    depth = 1;
  }
  let array = [];
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index]) && depth > 0) {
      array = array.concat(flatten(elements[index], depth - 1));
    } else if (elements[index] !== undefined) {
      array.push(elements[index]);
    }
  }
  return array;
}

module.exports = flatten;
