function reduce(elements, cb, startingValue) {
  let index = 0
  if (typeof startingValue === "undefined") {
    startingValue = elements[index]
    index++
  }

  for (index; index < elements.length; index++) {
    startingValue = cb(startingValue, elements[index], index, elements )
  }
  return startingValue
}



module.exports = reduce
