function map(array, cb = (value) => value) {
  const mapArray = []
  for (let index = 0; index < array.length; index++) {
    mapArray.push(cb(array[index], index, array))
  }
  return mapArray
}

module.exports = map
