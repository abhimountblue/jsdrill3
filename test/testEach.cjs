const each = require('../each.cjs')

const items = [1, 2, 3, 4, 5, 5]

each(items, (item, index) => item + 1)